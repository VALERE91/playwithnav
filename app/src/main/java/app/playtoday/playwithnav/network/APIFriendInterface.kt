package app.playtoday.playwithnav.network

import app.playtoday.playwithnav.model.Friend
import retrofit2.Call
import retrofit2.http.GET

interface APIFriendInterface {
    @GET("friends")
    fun getAllFriends() : Call<List<Friend>>
}