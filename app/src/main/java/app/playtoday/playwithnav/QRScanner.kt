package app.playtoday.playwithnav

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import java.lang.Exception
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class QRScanner : Fragment() {

    private lateinit var cameraExecutor: Executor

    private lateinit var barcodeOptions: BarcodeScannerOptions

    private lateinit var previewView: PreviewView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if(isCameraAllowed()){
            //Start the camera
            startCamera()
        } else {
            //Ask for permission and start the camera if granted
            if(activity is MainActivity){
                (activity as MainActivity).askForPermission(REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSION) { code, permission, granted ->
                    if(code == REQUEST_CODE_PERMISSION && granted) {
                        startCamera()
                    }
                }
            }
        }

        // Inflate the layout for this fragment
        val qrScannerView = inflater.inflate(R.layout.fragment_qr_scanne, container, false)

        previewView = qrScannerView.findViewById(R.id.camera_preview)

        return qrScannerView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cameraExecutor = Executors.newSingleThreadExecutor()
        barcodeOptions = BarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
            .build()
    }

    private fun isCameraAllowed() : Boolean{
        return ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener(Runnable {
            val cameraProvider = cameraProviderFuture.get()

            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(previewView.createSurfaceProvider())
                }

            val imageAnalyzer = ImageAnalysis.Builder()
                .setTargetRotation(previewView.display.rotation)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
                .also {
                    it.setAnalyzer(cameraExecutor, QRAnalyzer(barcodeOptions))
                }

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                cameraProvider.unbindAll()

                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageAnalyzer)
            } catch (ex: Exception){
                Log.e(TAG, "Unable to bind lifecycle", ex)
            }

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private class QRAnalyzer(val options: BarcodeScannerOptions) : ImageAnalysis.Analyzer {
        @SuppressLint("UnsafeExperimentalUsageError")
        override fun analyze(imageProxy: ImageProxy) {
            val media = imageProxy.image
            if(media == null){
                imageProxy.close()
                return
            }

            //We have an image
            val image = InputImage.fromMediaImage(media, imageProxy.imageInfo.rotationDegrees)
            val scanner = BarcodeScanning.getClient(options)
            scanner.process(image)
                .addOnSuccessListener { barcodes ->
                    //We got a barcode
                    for(barcode in barcodes){
                        Log.d(TAG, "QR Code : ${barcode.rawValue}")
                        //Call your API to add a friend
                        //Stop the camera
                        //Return back to friends list
                    }
                }
                .addOnFailureListener {
                    Log.e(TAG, "Failed to scan for QR", it)
                }
                .addOnCompleteListener {
                    imageProxy.close()
                }
        }

    }

    companion object {
        private const val TAG = "QRScannerFrag"
        private const val REQUEST_CODE_PERMISSION = 42
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }
}