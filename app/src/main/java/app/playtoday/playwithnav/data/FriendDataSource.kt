package app.playtoday.playwithnav.data

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import app.playtoday.playwithnav.cache.db.CacheDatabase
import app.playtoday.playwithnav.cache.worker.FriendCachingWorker
import app.playtoday.playwithnav.model.Friend
import app.playtoday.playwithnav.network.APIFriendInterface
import app.playtoday.playwithnav.network.ApiClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Response

class FriendDataSource(val context: Context, private val lifecycleOwner: LifecycleOwner) {
    private var apiInterface: APIFriendInterface? = null

    init {
        apiInterface = ApiClient.getApiClient().create(APIFriendInterface::class.java)
    }

    private fun getFriendsFromCache() : MutableLiveData<List<Friend>>
    {
        val data = MutableLiveData<List<Friend>>()

        Room.databaseBuilder(
            context,
            CacheDatabase::class.java,
            "cache_db"
        ).build()
            .friendDAO()
            .getAll().observe(lifecycleOwner, {
                if(data.value == null || data.value?.isEmpty()!!)
                    data.value = it
            })

        return data
    }

    fun getAllFriends() : LiveData<List<Friend>> {
        val data = getFriendsFromCache()

        apiInterface?.getAllFriends()?.enqueue(object : retrofit2.Callback<List<Friend>>{
            override fun onResponse(call: Call<List<Friend>>, response: Response<List<Friend>>) {
                val res = response.body()
                if(response.code() == 200)
                {
                    data.value = res

                    val workerData = Data.Builder()
                                        .putString(FriendCachingWorker.KEY_FRIENDS_LIST, Gson().toJson(res))
                                        .build()

                    val cachingWorkRequest = OneTimeWorkRequestBuilder<FriendCachingWorker>()
                        .setInputData(workerData)
                        .build()

                    WorkManager.getInstance(context).enqueue(cachingWorkRequest)
                }
            }

            override fun onFailure(call: Call<List<Friend>>, t: Throwable) {

            }
        })

        return data
    }
}