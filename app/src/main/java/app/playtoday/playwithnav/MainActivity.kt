package app.playtoday.playwithnav

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Html
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.text.HtmlCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.util.jar.Manifest


const val LOG_TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private var permissionsListener = emptyMap<Int, (Int, String, Boolean)->Unit>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navBar = findViewById<BottomNavigationView>(R.id.bottom_nav_view)
        val navController = findNavController(R.id.nav_host_frag)

        val appBarConfiguration = AppBarConfiguration(setOf(R.id.navigation_dashboard, R.id.navigation_friends, R.id.navigation_profile))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navBar.setupWithNavController(navController)
    }

    override fun onStart() {
        super.onStart()

        if(GoogleSignIn.getLastSignedInAccount(this) == null){
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            startActivity(intent)
        } else {
            Intent(this, HelloService::class.java). also { intent ->
                startService(intent)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.layout_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.logout_item -> {
                val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build()

                val googleClient = GoogleSignIn.getClient(this, gso)
                googleClient.signOut().addOnCompleteListener {
                    val intent = Intent(this, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    startActivity(intent)
                }
                true
            }
            R.id.share_item -> {
                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "text/html"
                intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>Hello world</p>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                startActivity(Intent.createChooser(intent, "Share with "))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun askForPermission(permissions : Array<String>, requestCode: Int, listener: (Int, String, Boolean) -> Unit){
        permissionsListener = permissionsListener + Pair(requestCode, listener)
        ActivityCompat.requestPermissions(this, permissions, requestCode)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        for((it, perm) in permissions.withIndex()){
            permissionsListener[requestCode]?.invoke(requestCode,
                    perm,
                    grantResults[it] == PackageManager.PERMISSION_GRANTED)
        }
    }
}