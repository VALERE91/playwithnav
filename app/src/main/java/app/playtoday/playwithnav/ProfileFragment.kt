package app.playtoday.playwithnav

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.annotation.RequiresApi
import com.google.zxing.WriterException

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {

    private lateinit var QRCodeImageView: ImageView
    private lateinit var QRCodeDataText: EditText

    private lateinit var QREncoder: QRGEncoder
    private lateinit var QRBitmap: Bitmap

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        QRCodeDataText = view.findViewById(R.id.qr_data_text)
        QRCodeImageView = view.findViewById(R.id.qr_code)

        view.findViewById<Button>(R.id.qr_generation_btn).setOnClickListener {
            if(TextUtils.isEmpty(QRCodeDataText.text)){
                Toast.makeText(requireContext(), "You need to enter the data to embark", Toast.LENGTH_LONG).show()
            } else {
                val displayMetrics = DisplayMetrics()
                activity?.display?.getRealMetrics(displayMetrics)

                val width = displayMetrics.widthPixels
                val height = displayMetrics.heightPixels

                var size = if(width < height) width else height
                size = size * 3 / 4

                QREncoder = QRGEncoder(QRCodeDataText.text?.toString(), null, QRGContents.Type.TEXT, size)

                try {
                    QRBitmap = QREncoder.encodeAsBitmap()
                    QRCodeImageView.setImageBitmap(QRBitmap)
                } catch (e: WriterException) {
                    Log.e("QRGenerator", e.toString())
                }
            }
        }

        return view
    }
}