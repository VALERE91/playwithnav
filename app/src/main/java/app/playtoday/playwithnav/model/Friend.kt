package app.playtoday.playwithnav.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Friend(
    @PrimaryKey val id: Int? = -1,
    @ColumnInfo(name = "username") val name: String? = "",
    @ColumnInfo(name = "created_at") val createdAt: Date? = null,
    @ColumnInfo(name = "avatar") val avatar: String? = ""
)