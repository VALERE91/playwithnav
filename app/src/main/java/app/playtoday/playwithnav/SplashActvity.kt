package app.playtoday.playwithnav

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper

class SplashActvity : AppCompatActivity() {
    private val SPLASH_TIME:Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_actvity)

        Handler(Looper.getMainLooper()).postDelayed({
              //Function
             startActivity(Intent(this, MainActivity::class.java))
        }, SPLASH_TIME)
    }
}