package app.playtoday.playwithnav.cache.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.playtoday.playwithnav.model.Friend

@Dao
interface FriendDAO {
    @Query("SELECT * FROM Friend")
    fun getAll(): LiveData<List<Friend>>

    @Insert
    fun insertAll(vararg friends: Friend)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: Friend)
}