package app.playtoday.playwithnav.cache.worker

import android.content.Context
import androidx.room.Room
import androidx.work.Worker
import androidx.work.WorkerParameters
import app.playtoday.playwithnav.cache.db.CacheDatabase
import app.playtoday.playwithnav.model.Friend
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class FriendCachingWorker(context: Context, params: WorkerParameters) : Worker(context, params) {
    override fun doWork(): Result {
        val friendDAO = Room.databaseBuilder(
            applicationContext,
            CacheDatabase::class.java,
            "cache_db"
        ).build().friendDAO()

        val itemType = object: TypeToken<List<Friend>>() {}.type
        val friends = Gson().fromJson<List<Friend>>(inputData.getString(KEY_FRIENDS_LIST), itemType)

        friends?.forEach {
            friendDAO.insert(it)
        }

        return Result.success()
    }

    companion object {
        const val KEY_FRIENDS_LIST = "FRIENDS_LIST"
    }
}