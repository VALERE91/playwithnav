package app.playtoday.playwithnav.cache.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import app.playtoday.playwithnav.cache.converters.CachingConverter
import app.playtoday.playwithnav.cache.dao.FriendDAO
import app.playtoday.playwithnav.model.Friend

@Database(entities = [Friend::class], version = 1)
@TypeConverters(CachingConverter::class)
abstract class CacheDatabase : RoomDatabase() {
    abstract fun friendDAO() : FriendDAO
}