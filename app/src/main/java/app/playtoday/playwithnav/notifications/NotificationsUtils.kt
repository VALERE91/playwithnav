package app.playtoday.playwithnav.notifications

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import app.playtoday.playwithnav.R

class NotificationsUtils(val context: Context) {
    companion object {
        var CHANNEL_IMPORTANT_ID = ""
        var CHANNEL_NOT_IMPORTANT_ID = ""
        var nextNotificationID = 0
    }

    init{
        CHANNEL_IMPORTANT_ID = context.getString(R.string.channel_important_id)
        CHANNEL_NOT_IMPORTANT_ID = context.getString(R.string.channel_not_important_id)

        createNotificationChannel(CHANNEL_IMPORTANT_ID,
            R.string.important_notif_channel_name,
            R.string.important_notif_channel_desc,
            NotificationManager.IMPORTANCE_HIGH)

        createNotificationChannel(
            CHANNEL_NOT_IMPORTANT_ID,
            R.string.not_important_notif_channel_name,
            R.string.not_important_notif_channel_desc,
            NotificationManager.IMPORTANCE_DEFAULT)
    }

    fun builder(channel: String) : NotificationCompat.Builder {
        return NotificationCompat.Builder(context, channel)
    }

    fun fireNotification(notif: Notification): Int{
        val notifID = nextNotificationID++

        with(NotificationManagerCompat.from(context)){
            notify(notifID, notif)
        }

        return notifID
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun createNotificationChannel(id: String, nameRes: Int, descriptionRes: Int, importance: Int){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channel = NotificationChannel(id, context.getString(nameRes), importance).apply {
                description = context.getString(descriptionRes)
            }

            val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}