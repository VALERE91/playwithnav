package app.playtoday.playwithnav.notifications

import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import app.playtoday.playwithnav.MainActivity
import app.playtoday.playwithnav.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseNotificationService : FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        super.onNewToken(token)

        FirebaseUtils.cached_token = token
        FirebaseUtils.cached_token_valid = true
    }

    override fun onMessageReceived(remoteMsg: RemoteMessage) {
        super.onMessageReceived(remoteMsg)

        remoteMsg.notification?.let{ notification ->
            val utils = NotificationsUtils(this)

            val intent = Intent(this, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

            val builder = utils.builder(NotificationsUtils.CHANNEL_IMPORTANT_ID)
                .setSmallIcon(R.drawable.ic_notification_important)
                .setContentTitle(notification.title)
                .setContentText(notification.body)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)

            utils.fireNotification(builder.build())
        }
    }
}