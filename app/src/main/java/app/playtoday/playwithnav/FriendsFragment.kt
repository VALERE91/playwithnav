package app.playtoday.playwithnav

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.playtoday.playwithnav.adapter.FriendAdapter
import app.playtoday.playwithnav.data.FriendDataSource

/**
 * A simple [Fragment] subclass.
 * Use the [FriendsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FriendsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val friendLayout = inflater.inflate(R.layout.fragment_friends, container, false)
        FriendDataSource(requireContext(), viewLifecycleOwner).getAllFriends().observe(viewLifecycleOwner, {
            if(it != null)
            {
                val recyclerView = friendLayout.findViewById<RecyclerView>(R.id.friend_recycler_view)
                recyclerView.adapter = FriendAdapter(it)
            }
            else
            {
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_LONG)
            }
        })

        friendLayout.findViewById<View>(R.id.add_friend_btn).setOnClickListener {
            val action = FriendsFragmentDirections.actionNavigationFriendsToQRScanner2()
            findNavController().navigate(action)
        }

        return friendLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}