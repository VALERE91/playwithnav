package app.playtoday.playwithnav.notifications

import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging

object FirebaseUtils {
    init {
        getRegistrationToken {

        }
    }

    fun getRegistrationToken(listener: (token:String) -> Unit){
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task->
            if(!task.isSuccessful){
                //LOG
                Log.w(LOG_TAG, "Fetching FCM token failed", task.exception)
                return@addOnCompleteListener
            }

            cached_token = task.result
            cached_token_valid = true
            listener(cached_token)
        }
    }

    var cached_token = ""
    var cached_token_valid = false

    private const val LOG_TAG = "app.playtoday.playwithnav.notifications.FirebaseUtils"
}